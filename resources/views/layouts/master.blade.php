 <!DOCTYPE html>
 <html>
 <head>
 	<meta charset="utf-8">
 	<meta name="viewport" content="width=device-width,initial-scale=1.0">

 	<!-- Title -->
 	<title>TecXra</title>

 	<!-- Favicon -->
 	<link rel="icon" href="{{asset('site/img/core-img/favicon.ico')}}">

 	<!-- Core Stylesheet -->
 	<link href="{{asset('site/css/style.css')}}" rel="stylesheet">
 	<!-- Responsive CSS -->
 	<link href="{{asset('site/css/responsive.css')}}" rel="stylesheet">



 	{{-- <link href="{{asset('site/css/animate.css')}}" rel="stylesheet">
 	<link href="{{asset('site/css/bootstrap.min.css')}}" rel="stylesheet">
 	<link href="{{asset('site/css/font-awesome.min.css')}}" rel="stylesheet">
 	<link href="{{asset('site/css/ionicons.min.css')}}" rel="stylesheet">
 	<link href="{{asset('site/css/magnific-popup.css')}}" rel="stylesheet">
 	<link href="{{asset('site/css/owl.carousel.min.css')}}" rel="stylesheet">
 	<link href="{{asset('site/css/slick.css')}}" rel="stylesheet">
 	<link href="{{asset('site/css/themify-icons.css')}}" rel="stylesheet"> --}}

 </head>
 <body>
 	<div id="app">

 		<!-- Preloader Start -->
 		<div id="preloader">
 			<div class="colorlib-load"></div>
 		</div>

 		<!-- ***** Header Area Start ***** -->
 		<header class="header_area animated">
 			<div class="container-fluid">
 				<div class="row align-items-center">
 					<!-- Menu Area Start -->
 					<div class="col-12 col-lg-10">
 						<div class="menu_area">
 							<nav class="navbar navbar-expand-lg navbar-light">
 								<!-- Logo -->
 								<a class="navbar-brand" href="#">TecXra.</a>
 								<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ca-navbar" aria-controls="ca-navbar" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
 								<!-- Menu Area -->
 								<div class="collapse navbar-collapse" id="ca-navbar">
 									<ul class="navbar-nav ml-auto" id="nav">
 										<li class="nav-item active"><a class="nav-link" href="#home">Home</a></li>
 										<li class="nav-item"><a class="nav-link" href="#about">About</a></li>
 										<li class="nav-item"><a class="nav-link" href="#features">Features</a></li>
 										<li class="nav-item"><a class="nav-link" href="#screenshot">Screenshot</a></li>
 										<li class="nav-item"><a class="nav-link" href="./pages/pricing.html">Pricing</a></li>
 										<li class="nav-item"><a class="nav-link" href="#testimonials">Testimonials</a></li>
 										<li class="nav-item"><a class="nav-link" href="#team">Team</a></li>
 										<li class="nav-item"><a class="nav-link" href="#contact">Contact</a></li>
 									</ul>
 								</div>
 							</nav>
 						</div>
 					</div>
 				</div>
 			</div>
 		</header>
 		<!-- ***** Header Area End ***** -->
 		@yield('content')

 		<!-- ***** Footer Area Start ***** -->
 		<footer class="footer-social-icon text-center section_padding_70 clearfix">
 			<!-- footer logo -->
 			<div class="footer-text">
 				<h2>TecXra.</h2>
 			</div>
 			<!-- social icon-->
 			<div class="footer-social-icon">
 				<a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
 				<a href="#"><i class="active fa fa-twitter" aria-hidden="true"></i></a>
 				<a href="#"> <i class="fa fa-instagram" aria-hidden="true"></i></a>
 				<a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
 			</div>
 			<div class="footer-menu">
 				<nav>
 					<ul>
 						<li><a href="#">About</a></li>
 						<li><a href="#">Terms &amp; Conditions</a></li>
 						<li><a href="#">Privacy Policy</a></li>
 						<li><a href="#">Contact</a></li>
 					</ul>
 				</nav>
 			</div>
 			<!-- Foooter Text-->
 			<div class="copyright-text">
 				<!-- ***** Removing this text is now allowed! This template is licensed under CC BY 3.0 ***** -->
 				<p>Copyright ©2014 TecXra Designed by TecXra</p>
 			</div>
 		</footer>
 	</div>
 	<!-- ***** Footer Area Start ***** -->

 	<!-- Jquery-2.2.4 JS -->
 	<script src="{{asset('site/js/jquery-2.2.4.min.js')}}"></script>
 	<!-- Popper js -->
 	<script src="{{asset('site/js/popper.min.js')}}"></script>
 	<!-- Bootstrap-4 Beta JS -->
 	<script src="{{asset('site/js/bootstrap.min.js')}}"></script>
 	<!-- All Plugins JS -->
 	<script src="{{asset('site/js/plugins.js')}}"></script>
 	<!-- Slick Slider Js-->
 	<script src="{{asset('site/js/slick.min.js')}}"></script>
 	<!-- Footer Reveal JS -->
 	<script src="{{asset('site/js/footer-reveal.min.js')}}"></script>
 	<!-- Active JS -->
 	<script src="{{asset('site/js/active.js')}}"></script>



 	<!-- built files will be auto injected -->

 </body>
 </html>





